#  AVS Device SDK

> Sample app



_tbd_



## References

* [AVS Device SDK](https://developer.amazon.com/alexa-voice-service/sdk)
* [alexa/avs-device-sdk](https://github.com/alexa/avs-device-sdk)
* [AlexaClientSDK Doc](https://alexa.github.io/avs-device-sdk/)
* [Linux Quick Start Guide](https://github.com/alexa/avs-device-sdk/wiki/Linux-Quick-Start-Guide)
* [Raspberry Pi Quick Start Guide](https://github.com/alexa/avs-device-sdk/wiki/Raspberry-Pi-Quick-Start-Guide)



